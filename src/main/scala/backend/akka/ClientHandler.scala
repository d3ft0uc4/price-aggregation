package backend.akka

import akka.actor.{ActorRef, Actor}
import akka.event.Logging
import akka.io.Tcp.{Received, PeerClosed, Write}
import akka.util.ByteString
import backend.persistance.Persistor
import org.json4s.Formats
import org.json4s.jackson.Serialization._
import backend.persistance.Persistor
import org.json4s.jackson.Serialization.write
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by d3ft0uch.
 */

class ClientHandler(sndr: ActorRef) extends Actor {
  val log = Logging(context.system, this)

  implicit def formats: Formats = org.json4s.DefaultFormats

  val candles = Persistor.getInitialCandles.map(write(_)).mkString("\n")
  sndr ! Write(ByteString(candles))
  context.system.scheduler.schedule((60000 - System.currentTimeMillis() % 60000) millis, 1 minutes)(sendPeriodic(sndr))

  private def sendPeriodic(sndr: ActorRef) = {
    sndr ! Write(ByteString(Persistor.getPeriodicCandles.map(write(_)).mkString("\n")))
  }

  def receive = {
    case Received(data) => sender() ! Write(data)
    case PeerClosed => context stop self
  }
}