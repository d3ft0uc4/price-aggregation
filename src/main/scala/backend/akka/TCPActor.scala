package backend.akka

import java.net.InetSocketAddress

import akka.actor.{ActorRef, Props, Actor}
import akka.event.Logging
import akka.io.{Tcp, IO}
import akka.io.Tcp._
import akka.util.ByteString


/**
 * Created by d3ft0uch.
 */

object TCPActor {
  def props(address: String, port: Int) = Props(new TCPActor(address, port))
}

class TCPActor(address: String, port: Int) extends Actor {
  implicit val fmts = org.json4s.DefaultFormats
  val log = Logging(context.system, this)

  override def preStart(): Unit = {
    log.info("Starting tcp net server")
    import context.system
    val opts = List(SO.KeepAlive(on = true), SO.TcpNoDelay(on = true))
    IO(Tcp) ! Bind(self, new InetSocketAddress(address, port), options = opts)
  }

  def receive = {
    case b@Bound(localAddress) => log.info(s"Bounded on port ${localAddress.getPort}")
    case CommandFailed(_: Bind) => context stop self
    case c@Connected(remote, local) =>
      log.info(s"New incoming tcp connection on server from port: ${remote.getPort}")
      val connection = sender()
      val handler = context.actorOf(Props(classOf[ClientHandler],sender()))
      connection ! Register(handler)
    case data: ByteString =>
      val str = data.utf8String
      log.info(s"Received $str")
    case x => println(x)
  }


}
