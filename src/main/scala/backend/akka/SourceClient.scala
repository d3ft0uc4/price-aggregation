package backend.akka

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, Props}
import akka.io.{IO, Tcp}
import akka.util.ByteString
import java.nio.ByteBuffer

import backend.persistance.Persistor

/**
 * Created by d3ft0uch.
 */
object SourceClient {
  def props(remote: InetSocketAddress) = Props(classOf[SourceClient], remote)
}

class SourceClient(remote: InetSocketAddress) extends Actor {

  import Tcp._
  import context.system

  IO(Tcp) ! Connect(remote)

  def receive = {
    case CommandFailed(_: Connect) =>
      context stop self
    case c@Connected(remote, local) =>
      val connection = sender()
      connection ! Register(self)
      context become {
        case data: ByteString =>
          connection ! Write(data)
        case CommandFailed(w: Write) =>
        case Received(data) =>
          val deal = Deal(data.toByteBuffer.array())
          Persistor.updateDeal(deal)
        case "close" =>
          connection ! Close
        case _: ConnectionClosed =>
          context stop self
      }
  }
}

case class Deal(timestamp: Long, ticker: String, price: Double, size: Int)

object Deal {
  //  [ LEN:2 ] [ TIMESTAMP:8 ] [ TICKER_LEN:2 ] [ TICKER:TICKER_LEN ] [ PRICE:8 ] [ SIZE:4 ]
  def apply(bytes: Array[Byte]): Deal = {
    if (bytes.length < 2) throw new RuntimeException(s"Got incorrect input: $bytes")
    val msgLength = ByteBuffer.wrap(Array[Byte](0, 0) ++ bytes.slice(0, 2)).getInt
    if (msgLength != bytes.length - 2) throw new RuntimeException(s"Got incorrect input: $bytes")
    val timestamp = ByteBuffer.wrap(bytes.slice(2, 10)).getLong
    val tickerLen = ByteBuffer.wrap(Array[Byte](0, 0) ++ bytes.slice(10, 12)).getInt
    val ticker = new String(ByteBuffer.wrap(bytes.slice(12, 12 + tickerLen)).array(), "ASCII")
    val price = ByteBuffer.wrap(bytes.slice(12 + tickerLen, 20 + tickerLen)).getDouble
    val size = ByteBuffer.wrap(bytes.slice(20 + tickerLen, 24 + tickerLen)).getInt
    Deal(timestamp, ticker, price, size)
  }
}

case class Candle(ticker: String, timestamp: String, open: Double, high: Double, low: Double, close: Double, volume: Int)