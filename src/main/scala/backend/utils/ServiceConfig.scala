package backend.utils

import java.io.FileInputStream
import java.util.Properties


object ServiceConfig {

  private val p = new Properties

  p.load(new FileInputStream("service.properties"))

  def getServerHost = getStringParam("server_host")

  def getServerPort = getIntParam("server_port")

  def getSourceHost = getStringParam("source_host")

  def getSourcePort = getIntParam("source_port")

  private def getStringParam(paramName: String, defaultValue: String = ""): String = {
    val paramValue = p.getProperty(paramName)
    if (paramValue == null) defaultValue else paramValue
  }

  private def getIntParam(paramName: String, defaultValue: Int = 0): Int = {
    val paramValue = p.getProperty(paramName)
    if (paramValue == null) defaultValue else paramValue.toInt
  }


}
