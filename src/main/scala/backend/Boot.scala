package backend

import java.net.InetSocketAddress

import _root_.akka.actor.ActorSystem
import backend.akka.{SourceClient, TCPActor}
import backend.utils.ServiceConfig

/**
 * Created by d3ft0uch.
 */
object Boot extends App {
  implicit val system = ActorSystem("AS")
  val actor = system.actorOf(TCPActor.props(ServiceConfig.getServerHost, ServiceConfig.getServerPort), name = "tcpActor")
  val src = system.actorOf(SourceClient.props(new InetSocketAddress(ServiceConfig.getSourceHost, ServiceConfig.getSourcePort)))
}
