package backend.persistance

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}
import java.util.concurrent.ConcurrentHashMap
import scala.collection.convert.decorateAsScala._
import backend.akka.{Candle, Deal}

/**
 * Created by d3ft0uch.
 */
object Persistor {
  val sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
  val candles = new ConcurrentHashMap[DealMeta, Values]().asScala
  val MINUTE = 60000 //millis

  def updateDeal(deal: Deal) = {
    val k = DealMeta((deal.timestamp / MINUTE) * MINUTE, deal.ticker)
    val v = candles.get(k) match {
      case Some(values) =>
        values.copy(
          volume = values.volume + deal.size,
          low = if (deal.price < values.low) deal.price else values.low,
          high = if (deal.price > values.high) deal.price else values.high,
          close = deal.price
        )
      case None => Values(deal.price, deal.price, deal.price, deal.price, deal.size)
    }
    candles.put(k, v)
  }

  def getInitialCandles: List[Candle] = getCandles(10)


  def getPeriodicCandles = getCandles(1)

  def getCandles(period: Int) = {
    val time = System.currentTimeMillis()
    candles.filter {
      case (k, v) => (1 * MINUTE < (time - k.date)) && ((time - k.date) < (period + 1) * MINUTE)
    }.map {
      case (k, v) => Candle(k.ticker, nearestMinute(k.date), v.open, v.high, v.low, v.close, v.volume)
    }.toList
  }

  case class DealMeta(date: Long, ticker: String)

  case class Values(open: Double, high: Double, low: Double, close: Double, volume: Int)

  def nearestMinute(timestamp: Long) = {
    val cal = Calendar.getInstance
    cal.setTime(new Date(timestamp))
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    sdf.format(cal.getTime)
  }
}
